/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wyszukiwarka;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author tomas
 */
public class SearchEngine {
    private String word;
    private String text;
    private boolean isCaseSensitive;
    private int counter;
    private Pattern pattern;


    public void setWord(String word) {
        this.word = word;
    }

    public SearchEngine() {

    }

    public String getWord() {
        return word;
    }

    public int getNumberOfApearances() {          
        
        counter = 0;
        
        if ("".equals(word)) {
            return counter;
        } else {
            Pattern pattern = isCaseSensitive ? Pattern.compile("\\b"+ word +"\\b") :
                Pattern.compile("\\b" + word + "\\b", Pattern.CASE_INSENSITIVE);
            
            Matcher matcher = pattern.matcher(text);

            while (matcher.find()) {
                counter++;
            }
            return counter;
        }
    }


    public void setText(String text) {
        this.text = text;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.isCaseSensitive = caseSensitive;
        
    }

    public boolean containsWord() {
        //
        return getNumberOfApearances() > 0 ? true : false;
    }

    @Override
    public String toString() {
        return containsWord() ?
                word + " występuje w tekście " + getNumberOfApearances() + " -krotnie" :
                word + " nie występuje w tekście";
    }
    
}
